package org.alexd.kio

import io.kotlintest.fail
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.collections.shouldHaveSize
import io.kotlintest.matchers.string.shouldContain
import io.kotlintest.matchers.string.shouldNotContain
import io.kotlintest.shouldBe
import io.kotlintest.shouldThrow
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineContext
import java.util.concurrent.atomic.AtomicReference

@UseExperimental(ObsoleteCoroutinesApi::class)
class KIOSpec : StringSpec() {

    init {
        "KIO just should wrap a pure value" {
            val io = KIO.just(42)
            io.unsafeRun() shouldBe 42
        }

        "KIO error should wrap an error" {
            val error = Exception("Boom!")
            val io = KIO.failed(error)

            shouldThrow<Exception> {
                io.unsafeRun()
            }.message shouldBe "Boom!" //TODO understand why it is not the same instance
        }

        "KIO should suspend a side effect, and run it on demand" {
            val list = mutableListOf<Int>()
            val io = KIO { list.add(0) }

            io.unsafeRun()
            io.unsafeRun()

            list shouldContainExactly listOf(0, 0)
        }

        "map operator allow to perform a pure transformation" {
            val io = KIO.just(42).map { it + 1 }
            io.unsafeRun() shouldBe 43
        }

        "flatMap operator allow to chain two effectful computation" {
            val io = KIO.just(42).flatMap { i -> KIO { i + 1 } }
            io.unsafeRun() shouldBe 43
        }

        "recursive flatMap chain should be stack-safe" {
            val list = mutableListOf<Int>()

            fun loop(n: Int): KIO<Unit> {
                val addToList = KIO { list += n }

                return addToList
                    .flatMap { addToList }
                    .flatMap { if (n > 1) loop(n - 1) else KIO.just(Unit) }
            }

            val io = loop(1000000)
            io.unsafeRun()
            list shouldHaveSize 2000000
        }

        "attempt should transform a faillible IO into a non fallible IO with result wrapped into Either, success case" {
            val io: KIO<Int> = KIO.just(42)

            when (val res = io.attempt().unsafeRun()) {
                is Left -> fail("should be Right")
                is Right -> res.right shouldBe 42
            }
        }

        "attempt should transform a faillible IO into a non fallible IO with result wrapped into Either, fail case" {
            val failingIO: KIO<Int> = KIO { error("Bim!") }

            when (val res = failingIO.attempt().unsafeRun()) {
                is Left -> res.left.message shouldBe "Bim!"
                is Right -> fail("should be Left")
            }
        }

        "recover should allow to recover from error, fail case" {
            val errorRef = AtomicReference<Throwable>()
            val io = KIO<Int> {
                error("Bim!")
            }.recover {
                errorRef.set(it)
                KIO.just(42)
            }

            io.unsafeRun() shouldBe 42
            errorRef.get()?.message shouldBe "Bim!"
        }

        "recover should allow  to recover from error, success case" {
            val io = KIO.just(42).recover {
                KIO.just(84)
            }

            io.unsafeRun() shouldBe 42
        }

        "bracket should allow to safely manage resource, success case" {
            val logs = mutableListOf<String>()
            val acquire = KIO {
                logs += "Acquire"
            }
            val release = KIO {
                logs += "Release"
            }

            val io = acquire.bracket({ release }) {
                KIO {
                    logs += "Work"
                    42
                }
            }

            io.unsafeRun() shouldBe 42
            logs shouldBe listOf("Acquire", "Work", "Release")
        }

        "bracket should allow to safely manage resource, fail case" {
            val logs = mutableListOf<String>()
            val acquire = KIO {
                logs += "Acquire"
            }
            val release = KIO {
                logs += "Release"
            }

            val io = acquire.bracket({ release }) {
                KIO {
                    logs += "Fail"
                    error("Booom")
                }
            }

            shouldThrow<Exception> { io.unsafeRun() }
            logs shouldBe listOf("Acquire", "Fail", "Release")
        }

        "bracket should allow to safely manage resource, cancellation case" {
            val ctx = TestCoroutineContext()
            val logs = mutableListOf<String>()
            val acquire = KIO {
                logs += "Acquire"
            }
            val release = KIO {
                logs += "Release"
            }

            val io = acquire.bracket({ release }) {
                KIO {
                    logs += "Work"
                    delay(1000)
                    logs += "Done"
                    42
                }
            }

            val deferred = async(ctx) { io.unsafeRun() }
            try {
                ctx.triggerActions()
                logs shouldBe listOf("Acquire", "Work")
                ctx.advanceTimeTo(500)
                deferred.cancel()
                ctx.triggerActions()
                ctx.advanceTimeTo(500)
                logs shouldBe listOf("Acquire", "Work", "Release")
            } finally {
                ctx.advanceTimeTo(10000)
            }

        }

        "parMap should allow to run two IO in parallel and compose their results, success case" {
            val ctx = TestCoroutineContext()
            val logs = mutableListOf<String>()

            val io = KIO {
                logs += "Started"
                delay(1000)
                logs += "Ended"
                42
            }

            val twoIO = io.parMap(io) { a, b -> a + b }

            val deferred = async(ctx) { twoIO.unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldBe listOf("Started", "Started")
                ctx.advanceTimeBy(1000)
                logs shouldBe listOf("Started", "Started", "Ended", "Ended")
                deferred.await() shouldBe 84
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        "parMap should allow to run two IO in parallel and compose their results, failure case" {
            val ctx = TestCoroutineContext()
            val logs = mutableListOf<String>()

            val io = KIO {
                try {
                    logs += "Started"
                    delay(1000)
                    logs += "Ended"
                    42
                } catch (e: CancellationException) {
                    logs += "Cancelled"
                    throw e
                }
            }
            val failingIO = KIO<Int> {
                logs += "Started"
                delay(500)
                logs += "Failed"
                error("Bam!")
            }

            val twoIO = io.parMap(failingIO) { a, b -> a + b }

            val deferred = async(ctx) { twoIO.attempt().unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldBe listOf("Started", "Started")
                ctx.advanceTimeBy(500)
                logs shouldBe listOf("Started", "Started", "Failed", "Cancelled")
                when (val res = deferred.await()) {
                    is Left -> res.left.message shouldBe "Bam!"
                    is Right -> fail("should be Left")
                }
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        "parMap should allow to run two IO in parallel and compose their results, cancellation case" {
            val ctx = TestCoroutineContext()
            val logs = mutableListOf<String>()

            val io = KIO {
                try {
                    logs += "Started"
                    delay(1000)
                    logs += "Ended"
                    42
                } catch (e: CancellationException) {
                    logs += "Cancelled"
                    throw e
                }
            }

            val twoIO = io.parMap(io) { a, b -> a + b }
            val deferred = async(ctx) { twoIO.unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldBe listOf("Started", "Started")
                ctx.advanceTimeBy(500)
                deferred.cancel()
                ctx.triggerActions()
                logs shouldBe listOf("Started", "Started", "Cancelled", "Cancelled")
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        "runOn should allow to run an effectful computation in a coroutine context" {
            val logs = mutableListOf<String>()
            val io = KIO {
                logs += Thread.currentThread().name
            }

            io.unsafeRun()
            io.runOn(newSingleThreadContext("fubar")).unsafeRun()

            logs shouldHaveSize 2
            logs[0] shouldNotContain "fubar"
            logs[1] shouldContain "fubar"
        }

        """parTraverse should allow to apply an effect to a list, and create an effect of the list of results,
            effects should be run in parallel, without restriction on concurrency""" {
            val ctx = TestCoroutineContext()
            val items = 1..1000
            val logs = mutableListOf<String>()

            fun task(i: Int) = KIO {
                logs += "Started"
                delay(1000)
                logs += "Done"
                i
            }

            val io = items.parTraverse(::task)
            val deferred = async(ctx) { io.unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldHaveSize 1000
                logs.all { it == "Started" } shouldBe true

                ctx.advanceTimeTo(1000)
                logs shouldHaveSize 2000
                logs.drop(1000).all { it == "Done" } shouldBe true

                deferred.await() shouldContainExactly items.toList()
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        """parTraverse should allow to apply an effect to a list, and create an effect of the list of results,
            effects should be run in parallel, and cancelled if one of them failed""" {
            val ctx = TestCoroutineContext()
            val items = 1..1000
            val logs = mutableListOf<String>()

            fun task(i: Int) = KIO {
                try {
                    logs += "Started"
                    if (i == 500) {
                        delay(500)
                        logs += "Failed"
                        error("Sorry")
                    }
                    delay(1000)
                    logs += "Done"
                    i
                } catch (e: CancellationException) {
                    logs += "Cancelled"
                    throw e
                }
            }

            val io = items.parTraverse(::task)
            val deferred = async(ctx) { io.attempt().unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldHaveSize 1000
                logs.all { it == "Started" } shouldBe true

                ctx.advanceTimeTo(500)
                logs shouldHaveSize 2000
                logs[1000] shouldBe "Failed"
                logs.drop(1001).all { it == "Cancelled" } shouldBe true

                when (val res = deferred.await()) {
                    is Left -> res.left.message shouldBe "Sorry"
                    is Right -> fail("should be Left")
                }
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        """parTraverse should allow to apply an effect to a list, and create an effect of the list of results,
            effects should be run in parallel, and be cancelled all together""" {
            val ctx = TestCoroutineContext()
            val items = 1..1000
            val logs = mutableListOf<String>()

            fun task(i: Int) = KIO {
                try {
                    logs += "Started"
                    delay(1000)
                    logs += "Done"
                    i
                } catch (e: CancellationException) {
                    logs += "Cancelled"
                    throw e
                }
            }

            val io = items.parTraverse(::task)
            val deferred = async(ctx) { io.attempt().unsafeRun() }

            try {
                ctx.triggerActions()
                logs shouldHaveSize 1000
                logs.all { it == "Started" } shouldBe true

                ctx.advanceTimeTo(500)
                deferred.cancel()
                ctx.triggerActions()

                logs shouldHaveSize 2000
                logs.drop(1000).all { it == "Cancelled" } shouldBe true
            } finally {
                ctx.advanceTimeTo(10000)
            }
        }

        """parTraverseN should allow to apply an effect to a list, and create an effect of the list of results,
            effects should be run in parallel, with limitation on concurrency""" {
            val ctx = TestCoroutineContext()
            val items = 1..1000
            val logs = mutableListOf<Pair<Long, String>>()

            fun task(i: Int) = KIO {
                logs += ctx.now() to "Started"
                delay(1000)
                logs += ctx.now() to "Done"
                i
            }

            val io = items.parTraverseN(100, ::task)
            val deferred = async(ctx) { io.unsafeRun() }

            try {
                ctx.advanceTimeTo(10000)
                logs shouldHaveSize 2000

                val timeline = logs.groupBy({ it.first }, { it.second })

                val concurrencyTimeline = timeline.keys.sorted().fold(listOf(0)) { concurrents, time ->
                    val events = timeline.getValue(time)
                    val started = events.count { it == "Started" }
                    val ended = events.count { it == "Done" }
                    val concurrent = concurrents.last()
                    concurrents + (concurrent + started - ended)
                }

                concurrencyTimeline shouldBe listOf(0, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 0)
                deferred.await() shouldContainExactly items.toList()
            } finally {
                ctx.advanceTimeTo(100000)
            }
        }

        "KIO syntax should 'tolerate' to run other IO in a IO suspended block " {
            val logs = mutableListOf<String>()
            val io = KIO { logs += "Hello" }

            val directStyleIO = KIO {
                io.run()
                io.run()
            }

            logs shouldHaveSize 0
            directStyleIO.unsafeRun()
            logs shouldBe listOf("Hello", "Hello")
        }
    }
}